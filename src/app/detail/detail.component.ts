import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { YoutubeService } from '../shared/services/youtube/youtube.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'ic-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  private subscriptions: Subscription[] = [];

  public loading: boolean = true;
  public video: any = [];

  constructor(
    private route: ActivatedRoute,
    private youtubeService: YoutubeService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.show();
  }

  show() {
    this.subscriptions.push(
      this.youtubeService.show(this.route.snapshot.params.id).subscribe(res => {
        this.loading = false;
        if (res.items[0]) {
          this.video = res.items[0];
        }
      })
    )
  }
  
  videoUrl(id: string) {
    const dangerousVideoUrl = `${environment.embedYoutube}/${id}`;
    return this.sanitizer.bypassSecurityTrustResourceUrl(dangerousVideoUrl);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

}
