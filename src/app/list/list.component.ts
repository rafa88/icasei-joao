import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { YoutubeService } from '../shared/services/youtube/youtube.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'ic-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  private subscriptions: Subscription[] = [];

  public searchForm: FormGroup;
  public loaded: boolean = false;
  public loading: boolean = false;
  public nextPageToken;
  public prevPageToken;
  public totalResults: number;
  public videos: any = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private youtubeService: YoutubeService
  ) { }

  ngOnInit() {
    if (this.isSearch()){
      this.search(this.route.snapshot.queryParams);
    }
    this.listeningChangeRoutes();
    this.createFormClient();
  }

  createFormClient() {
    this.searchForm = this.formBuilder.group({
      q: [this.route.snapshot.queryParams.q || '', [Validators.required]],
    });
  }

  listeningChangeRoutes(): void {
    this.subscriptions.push(
      this.router.events
        .filter(res => res instanceof NavigationEnd)
        .subscribe((res: NavigationEnd) => {
          this.search(this.route.snapshot.queryParams);
        })
    );
  }

  search(params?: any) {
    this.loaded = false;
    this.loading = !this.loading;
    this.subscriptions.push(
      this.youtubeService.search(params).subscribe(res => {
        this.videos = res.items;
        this.nextPageToken = res.nextPageToken || null;
        this.prevPageToken = res.prevPageToken || null;
        this.totalResults = res.pageInfo.totalResults;
        this.loaded = true;
        this.loading = !this.loading;
      })
    )
  }

  isSearch(): boolean {
    return this.route.snapshot.queryParams.q ? true : false;
  }

  sendForm() {
    if ( this.searchForm.valid ) {
      this.router.navigate([`/`], { queryParams: this.searchForm.value });
    }
  }

  next() {
    if (this.nextPageToken) {
      this.router.navigate([`/`], { queryParams: { pageToken: this.nextPageToken }, queryParamsHandling: "merge" });
    }
  }

  prev() {
    if (this.prevPageToken) {
      this.router.navigate([`/`], { queryParams: { pageToken: this.prevPageToken }, queryParamsHandling: "merge" });
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

}
