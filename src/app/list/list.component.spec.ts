import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { ListComponent } from './list.component';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

describe('ListComponent', () => {

  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, ActivatedRoute],
      declarations: [FormGroup, ListComponent],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    });

    fixture = TestBed.createComponent(ListComponent);

    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it('Formulário vazio', () => {
    expect(component.searchForm.valid).toBeFalsy();
  });

  it('Validando o input de buscar', () => {
    let errors = {};
    let password = component.searchForm.controls['q'];

    password.setValue("");
    errors = password.errors || {};
    expect(errors['required']).toBeFalsy();

    // Setando valor para busca
    password.setValue("red hot chili peppers");
    errors = password.errors || {};
    expect(errors['required']).toBeFalsy();
  });

  it('Enviando um formulário', () => {
    expect(component.searchForm.valid).toBeFalsy();
    component.searchForm.controls['q'].setValue("red hot chili peppers");
    expect(component.searchForm.valid).toBeTruthy();
  });

});
