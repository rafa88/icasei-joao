import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HeaderService } from './services/header/header.service';
import { YoutubeService } from './services/youtube/youtube.service';
import { VideoComponent } from './video/video.component';
import { NoResultsFoundComponent } from './no-results-found/no-results-found.component';
import { CarouselComponent } from './carousel/carousel.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    VideoComponent,
    NoResultsFoundComponent,
    CarouselComponent
  ],
  exports: [
    VideoComponent,
    NoResultsFoundComponent,
    CarouselComponent
  ],
  providers: [
    HeaderService,
    YoutubeService
  ]
})
export class SharedModule { }
