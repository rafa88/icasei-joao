import { TestBed, inject } from '@angular/core/testing';

import { YoutubeService } from './youtube.service';
import { Http, HttpModule } from '@angular/http';
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
describe('YoutubeService', () => {

  let service: YoutubeService;
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [Http, HttpModule],
      providers: [YoutubeService]
    });
  });

  it('Lista de videos', (done) => {
    service.search({ q: 'red hot chili peppers' }).subscribe((response) => {
      expect(response);
      done();
    });
  });

});
