import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { HeaderService } from '../header/header.service';
import { environment } from '../../../../environments/environment';
import { DataHandler } from '../../helpers/data-handler';
import { Video } from '../../models/video/video.model';

@Injectable()
export class YoutubeService {

  constructor(
    private http: Http,
    private headerService: HeaderService,
  ) { }

  search(params?: any): Observable<any> {
    return this.http.get(`${environment.endpoint}/search?part=id,snippet&maxResults=${environment.maxResultsYoutube}&order=date&key=${environment.apiKeyYouTube}`, this.headerService.setHeader(params))
      .map(DataHandler.handlerData)
      .catch(DataHandler.handlerError);
  }

  show(id): Observable<Video> {
    return this.http.get(`${environment.endpoint}/videos?id=${id}&part=snippet,statistics&key=${environment.apiKeyYouTube}`, this.headerService.setHeader())
      .map(res => {
        return new Video(res.json())
      })
      .catch(DataHandler.handlerError);
  }

}
