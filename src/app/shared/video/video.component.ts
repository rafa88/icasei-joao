import { Component, OnInit, Input } from '@angular/core';
import { Video } from '../models/video/video.model';

@Component({
  selector: 'ic-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {

  @Input() video;

  constructor() { }

  ngOnInit() {
  }

}
