import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'ic-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  @Input() videos;
  @Input() items: number = 1;
  @Input() autoplayTimeout: number = 1000;

  autoplayTimer;
  videosClone = [];
  width: number;
  widthItem: number = 225;
  translateX: number = 0;
  count: number = 0;
  fixedWidth: number = 600;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.videos = changes.videos.currentValue;

    var clone = JSON.parse(JSON.stringify( changes.videos.currentValue ));
    this.videosClone = clone;
    clone.forEach(e => {
      this.videosClone.push(e);
    });
    this.reload();
  }
  
  reload() {
    this.count = 1;
    this.translateX = 0;
    this.setWidth();
    this.stopAutoplayTimer();
    this.startAutoplay();
  }

  mouseEnter() {
    this.stopAutoplayTimer();
  }

  mouseLeave() {
    this.startAutoplay();
  }

  setWidth() {
    this.widthItem = this.fixedWidth / this.items;
    this.width = (this.widthItem * this.videosClone.length) + (5 * this.widthItem);
  }

  move(setp) {
    this.translateX = ((this.widthItem + 5) * setp) * -1;
  }

  startAutoplay() {
    this.autoplayTimer = setInterval(() => {
      if (this.count == (this.items)) {
        this.count = 0;
      } else {
        this.count = this.count + 1;
      }
      this.move( this.count );
    }, this.autoplayTimeout);
  }

  stopAutoplayTimer() {
    clearInterval(this.autoplayTimer);
  }

}
