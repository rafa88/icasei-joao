import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ic-no-results-found',
  templateUrl: './no-results-found.component.html',
  styleUrls: ['./no-results-found.component.scss']
})
export class NoResultsFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
