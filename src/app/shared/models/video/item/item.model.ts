import { Base } from "../../base.model";
import { Relational } from "../../../decorators";
import { Id } from "./id/id.model";
import { Snippet } from "./snippet/snippet.model";

export class Item extends Base {

  public kind: string;
  public etag: string;

  private _id: Id;
  private _snippet: Snippet;

  constructor(params?: any) { super(params); }

  @Relational()
  @Reflect.metadata('design:type', Id)
  get id() { return this._id; }
  set id(value: any) { }

  @Relational()
  @Reflect.metadata('design:type', Snippet)
  get snippet() { return this._snippet; }
  set snippet(value: any) { }

}
