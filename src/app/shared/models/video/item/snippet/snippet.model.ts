import { Base } from "../../../base.model";

// import { Relational } from '../../decorators';
// import { Base } from '../base.model';

export class Snippet extends Base {

  public publishedAt: string;
  public channelId: string;
  public title: string;
  public description: string;
  public channelTitle: string;
  public liveBroadcastContent: string;

  // private _id: Id;
  // private _snippet: Snippet;

  constructor(params?: any) { super(params); }

  // @Relational()
  // @Reflect.metadata('design:type', Component)
  // get body_type() { return this._body_type; }
  // set body_type(value: any) { }


}
