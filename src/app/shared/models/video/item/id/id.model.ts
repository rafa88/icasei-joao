import { Base } from "../../../base.model";

export class Id extends Base {

  public kind: string;
  public videoId: string;

  constructor(params?: any) { super(params); }

}
