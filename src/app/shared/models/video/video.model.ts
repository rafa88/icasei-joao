import { Base } from '../base.model';
import { Relational, RelationalList, RelationalAbstract, EnumFunction } from '../../decorators';
import { Item } from './item/item.model';

export class Video extends Base {

  public id: string;
  public kind: string;
  public etag: string;
  public nextPageToken: string;
  public prevPageToken: string;
  public regionCode: string;

  private _items: Item;

  @Relational()
  @Reflect.metadata('design:type', Item)
  get items() { return this._items; }

}
