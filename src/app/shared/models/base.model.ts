export class Base {

  constructor(params?: any) {
    if (params) {  this.setParams(params); }
  }

  setParams(params: any): void {
    for (const [key, value] of Object.entries(params)) {
      this[key] = value;
    }
  }

  inspect(): void {
    for (const [key, value] of Object.entries(this)) {
      console.log(key, value);
    }
  }

  className(): string {
    return this.constructor.name;
  }

  isA(klass: string): boolean {
    return this.constructor.name === klass;
  }

}
