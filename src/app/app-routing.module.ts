import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';

const appRoutes: Routes = [
  { path: '', component: ListComponent },
  { path: ':id', component: DetailComponent },
  { path: '**', redirectTo: '/404' }
  // { path: '404', component: NotFoundComponent, data: { title: 'Página não Encontrada' }, },
  // { path: '500', component: InternalServerErrorComponent, data: { title: 'Ocorreu um erro' }, },
  // { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
